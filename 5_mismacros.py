import uno
import easymacro as app


def main():
	# Hacer referencia al documento activo
	doc = XSCRIPTCONTEXT.getDocument()
	
	# Hacer referencia a la hoja activa
	# ~ hoja = doc.CurrentController.ActiveSheet
	
	# Acceder por posición a la hoja. PUNTO #6 
	hoja = doc.Sheet[0]
	
	
	# ~ celda1 = hoja['A1']
	# ~ celda1.String = 'Tom Hardy'
	
	# ~ '''
	# ~ Haciendo referencia a las celdas, con su respectivo índice
	# ~ '''
	# ~ celda2 = hoja[0,1]
	# ~ celda2.String = 'Batman'
	
	# ~ celda3 = hoja.getCellByPosition(2,0)
	# ~ celda3.String = 'Ironman'
	
	'''
	Rango en una tupla
	'''
	# ~ lista = (1,2,3, "Hola", "Mundo")
	# ~ app.msgbox(type(lista))
	# ~ app.msgbox(lista)
	
	'''
	Extraer datos de un rango
	'''
	# ~ rango = hoja['A1:C3']
	# ~ datos = rango.DataArray
	# ~ app.msgbox(rango)
	# ~ app.msgbox(type(datos))
	
	'''
	Tupla sencilla
	'''
	# ~ rango = hoja['A1:C3']
	# ~ datos = (
		# ~ ("A1", "B1", "C1"),
		# ~ ("A2", 2, "C2"),
		# ~ ("A3", 100.5, "C3"),
	# ~ )
	
	# Data array permite recuperar o establecer el valor de una celda
	
	# para establecer, la variable va del lado derecho
	# ~ rango.DataArray = datos
	
	# para obtener, la variable va del lado izquierdo.
	# ~ datos = rango.DataArray
	
	'''
	Ejemplo 2: tupla sencilla
	'''
	# ~ rango = hoja['A1:C3']
	# ~ datos = (1,2,3,4,5,6,7,8,9,0)
	 
	# ~ app.msgbox(datos)
	
	# ~ # Recorte de la tupla 
	# ~ # Slice
	# ~ app.msgbox(datos[2:])
	# ~ app.msgbox(datos[:6])
	# ~ app.msgbox(datos[3:7])
	   
	'''
	Definir rango: CON INDICES
	'''
	# ~ rango = hoja['A1:C3']
	# ~ datos = (
		# ~ ("A1", "B1", "C1"),
		# ~ ("A2", 2, "C2"),
		# ~ ("A3", 100.5, "C3"),
	# ~ )
	
	# ~ rango = hoja[0:3,0:3]
	# ~ # Propiedad que permite ver por mensaje donde nos encontramos
	# ~ app.msgbox(rango.AbsoluteName)
	
	# ~ rango.DataArray = datos
	
	'''
	Capturar datos y copiar los datos ingresados en otra hoja
	'''
	
	# cambiar nombre de la hoja
	hoja.Name = 'Ingreso'
	
	
	return
