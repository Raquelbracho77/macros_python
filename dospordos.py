import uno
import easymacro as app

def principal():
	# ~ # acceder al documento activo
	# ~ doc = XSCRIPTCONTEXT.getDocument()
	# ~ # Acceder a la hoja activa
	# ~ hoja = doc.CurrentController.ActiveSheet
	
	# ~ fila = 9
	# ~ celda = hoja[fila, hoja.Columns.Count-1]
	
	
	
	# ~ nombre = 'Tom Hardy'
	# ~ celda.String=nombre
	
	
	# ~ return
	
	doc = XSCRIPTCONTEXT.getDocument()
	seleccion = doc.CurrentController.Selection
	
	# ~ # Solución 1: División del code por barra inversa
	# ~ if seleccion.ImplementationName == 'ScCellRangeObj' and \
		# ~ seleccion.Rows.Count == 2 and seleccion.Columns.Count == 2:
		# ~ # MiEstilo debe configurarse en la hoja 
		# ~ seleccion.CellStyle = 'MiEstilo'
	# ~ else:
			# ~ app.msgbox("Debes seleccionar un rango de celdas")
	# ~ return
	
	# Solución 2:
	# ~ if seleccion.ImplementationName != 'ScCellRangeObj':
		# ~ app.msgbox("Debes seleccionar un rango de celdas")
		# ~ return
		
	# ~ if seleccion.Rows.Count != 2: 
		# ~ app.msgbox("Selecciona solo dos filas")
		# ~ return
		
	# ~ if seleccion.Columns.Count != 2:
		# ~ app.msgbox("Selecciona solo dos columnas")
		# ~ return
	
	
	# ~ seleccion.CellStyle = 'MiEstilo'
	# ~ return
	
	# 3
	
	if seleccion.ImplementationName != 'ScCellRangeObj':
		app.msgbox("Debes seleccionar un rango 4 columnas para aplicar el estilo")
		return
	
	if seleccion.Columns.Count < 4:
		app.msgbox("Debes seleccionar un rango 4 columnas")
		return
	
	if seleccion.Columns.Count > 4:
		app.msgbox("Debes seleccionar un rango 4 columnas")
		return
	
	if seleccion.Columns.Count == 4: 
		
		plantilla = (
			('Nombre', 'Apellido', 'Correo', 'Teléfono'),
		)
	
		seleccion.DataArray = plantilla
		seleccion.CellStyle = 'MiEstilo'
	
	return

def sin_easymacro():
	doc = XSCRIPTCONTEXT.getDocument()
	hoja = doc.CurrentController.ActiveSheet
	celda_origen = hoja['A1']
	celda_destino = hoja['C1']
	celda_destino.Value = celda_origen.Value
	return
	
# ~ def _primer_macro():
	#app.msgbox("Hola mundo desde LibreOffice con Python")
	# ~ doc= XSCRIPTCONTEXT.getDocument()
	# ~ cell = doc.Sheets[0]['A1']
	# ~ cell.String = 'En Calc'
	
	# ~ cell = doc.Text
	
	# ~ cell.String = "En Writer"
	
	# ~ return
	
def _segunda_macro(argumento):
	#Haciendo referencia a la celda A1 con cell
	# ~ cell = app.get_cell()
	# ~ cell.value = "Hola " + argumento
	
	'''CLASE 003'''
	
	# ~ doc = XSCRIPTCONTEXT.getDocument()
	# ~ seleccion = doc.CurrentController.Selection
	
	# ~ if seleccion.ImplementationName == 'ScCellObj':
		# ~ # Estilo en la celda
		# ~ seleccion.String = 'La vida sencilla'
		# ~ seleccion.CellStyle = 'MiEstilo'
		
	# ~ elif seleccion.ImplementationName == 'ScCellRangeObj':
		# ~ datos = (
			# ~ ('uno', 'dos'),
			# ~ ('tres', 'cuatro'),
		# ~ )
		
		# ~ app.msgbox(seleccion.Rows.Count)
		# ~ app.msgbox(seleccion.Columns.Count)
		
		# ~ seleccion.DataArray = datos
		# ~ seleccion.CellStyle = 'MiEstilo'
	# ~ else:
		# ~ app.msgbox("Selecciona una celda")
	

	
	
	# ~ '''Para acceder a la hoja sin la libreria easymacro'''
	# ~ doc = XSCRIPTCONTEXT.getDocument()
	# ~ cell = doc.Sheets[0]['A2']
	# ~ cell.String = 'Hola mundo 2'
	# ~ #Para valor numérico
	# ~ cell = doc.Sheets[0]['A3']
	# ~ cell.Formula = '=RAND'
	# ~ cell.String =  Para string
	# ~ cell.Value = 100 Para valores numericos
	return



#g_exportedScripts = (principal)

