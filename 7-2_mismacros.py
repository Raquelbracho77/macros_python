import uno
import easymacro as app


def crear_instancia(nombre):
	contexto = uno.getComponentContext()
	service_manager = contexto.getServiceManager()
	objeto = service_manager.createInstanceWithContext(nombre, contexto)
	return objeto
	
	
def crear_documento(tipo='calc'):
	nombre_servicio = 'com.sun.star.frame.Desktop'
	desktop = crear_instancia(nombre_servicio)
	
	url = f'private:factory/s{tipo}'
	documento = desktop.loadComponentFromURL(url, '_default', 0, ())
	# ~ app.msgbox(documento.Title)
	return documento

'''
main de ejemplo 
'''
# ~ def main():
	# ~ nombre_servicio = 'com.sun.star.frame.Desktop'
	# ~ desktop = crear_instancia(nombre_servicio)
	
	# ~ url = 'private:factory/scalc'
	# ~ doc = desktop.loadComponentFromURL(url, '_default', 0, ())
	# ~ app.msgbox(doc.Title)
	# ~ return

def main():
	doc = crear_documento()
	app.msgbox(doc.Title)
	return
