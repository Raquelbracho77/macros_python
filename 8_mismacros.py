import uno
from com.sun.star.beans import PropertyValue
import easymacro as app


def crear_instancia(nombre):
	contexto = uno.getComponentContext()
	service_manager = contexto.getServiceManager()
	objeto = service_manager.createInstanceWithContext(nombre, contexto)
	return objeto
	
	
def crear_documento(tipo='calc'):
	nombre_servicio = 'com.sun.star.frame.Desktop'
	desktop = crear_instancia(nombre_servicio)
	
	url = f'private:factory/s{tipo}'
	documento = desktop.loadComponentFromURL(url, '_default', 0, ())
	# ~ app.msgbox(documento.Title)
	return documento


def establecer_propiedades(argumentos):
	# lista temporal
	tmp = []
	for clave, valor in argumentos.items():
		tmp.append( = (PropertyValue(Name = 'Hidden', Value = True))
	return


# agregamos los argumentos que por default seran una tupla vacía
def abrir_documento(ruta, argumentos = ()):
	nombre_servicio = 'com.sun.star.frame.Desktop'
	desktop = crear_instancia(nombre_servicio)
	
	# ~ app.msgbox(ruta)
	url = uno.systemPathToFileUrl(ruta)
	# ~ app.msgbox(url)
	
	documento = desktop.loadComponentFromURL(url, '_default', 0, argumentos)
	# ~ app.msgbox(documento.Title)
	return documento

def main():
	ruta = '/home/raquel/.config/libreoffice/4/user/Scripts/python/origen_destino.ods'
	
	# Abrir el documento en la ruta de forma oculta
	argumentos = (PropertyValue(Name = 'Hidden', Value = True),)
	
	doc = abrir_documento(ruta, (argumentos,))
	
	app.msgbox(doc.Title)
	
	doc.close(True)
	
	return
