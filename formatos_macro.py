import uno
import easymacro as app

def principal():
	# acceder al documento activo
	doc = XSCRIPTCONTEXT.getDocument()
	# Acceder a la hoja activa
	hoja = doc.CurrentController.ActiveSheet
	# Asignando la celda A1
	celda = hoja['A1']
	# Variable con nombre y texto
	nombre = 'Tom Hardy'
	# objeto celda, propiedad string, asignando la cadena de texto nombre
	celda.String = nombre
	celda = hoja['A2']
	celda.String = str(type(nombre))
	
	
	celda = hoja['B1']
	edad = 35
	celda.Value = edad
	celda = hoja['B2']
	celda.String = str(type(edad))
	
	celda = hoja['D1']
	valor = 100.5
	celda.Value = valor
	celda = hoja['D2']
	celda.String = str(type(valor))
	
	celda = hoja['A28']
	celda.String = nombre + ' ' + str(edad)
	
	
	celda = hoja['A29']
	celda.String = f'Nombre: {nombre} - Edad: {edad}'
	
	# con msgbox
	celda = hoja['A30']
	app.msgbox(f'Nombre: {nombre} Edad: {edad}')
	
	
	return


def sin_easymacro():
	doc = XSCRIPTCONTEXT.getDocument()
	hoja = doc.CurrentController.ActiveSheet
	celda_origen = hoja['A1']
	celda_destino = hoja['C1']
	celda_destino.Value = celda_origen.Value
	return
