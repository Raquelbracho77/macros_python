import uno
import easymacro as app

def main():
	doc = XSCRIPTCONTEXT.getDocument()
	
	hoja_origen = doc.Sheets['Origen']
	hoja_destino= doc.Sheets['Destino']
	
	celda_origen = hoja_origen['A1']
	
	# crear objeto a partir de la hoja (puede ser cualquiera) 
	# esto es un cursor para indicar el rango
	cursor = hoja_origen.createCursorByRange(celda_origen)
	
	# ~ app.msgbox(celda_origen.AbsoluteName)
	# ~ app.msgbox(cursor.AbsoluteName)
	
	cursor.collapseToCurrentRegion()
	
	# ~ app.msgbox(cursor.AbsoluteName)
	
	'''
	Traer los datos creando un objeto indicando el rango de datos a usar
	en este caso no se utiliza el encabezado de la hoja sino mostrar los
	datos a partir de la posición 1
	'''
	data = cursor.DataArray[1:]
	# ~ app.msgbox(data)
	
	
	'''
	DESTINO DE LOS DATOS
	copiar a otra hoja
	'''
	celda_destino = hoja_destino['A1']
	cursor = hoja_destino.createCursorByRange(celda_destino)
	cursor.collapseToCurrentRegion()
	
	fila_nueva = cursor.RangeAddress.EndRow+1
	
	filas = fila_nueva + len(data)
	rango_destino = hoja_destino[fila_nueva:filas, 0:2]
	rango_destino.DataArray = data 
	
	'''
	BORRAR
	'''
	# seleccionando la fila 1 hasta donde haya datos
	rango_origen = hoja_origen[1:filas, 0:2]
	# limpiar 
	rango_origen.clearContents(31)
	
	return
