import uno
import easymacro as app


def main():
	doc = XSCRIPTCONTEXT.getDocument()
	hoja = doc.Sheets[0]
	
	# ~ app.msgbox(app.INFO_DEBUG)
	
	'''
	CICLOS
	'''
	
	# ~ lista = [1, 2, 3, 4, 5]
	
	# ~ for i in lista:
		# ~ app.msgbox(i)
	# Para saber los datos de la celda, fila, colummna, hoja...
	# ~ app.msgbox(celda.AbsoluteName)
	
	'''
	Verificar si una celda en fila está vacía o la última celda con datos
	'''
	# ~ celda = hoja[0,0]
	# ~ fila = 1
	# ~ while celda.String.strip():
		# ~ celda = hoja[fila, 0]
		# ~ fila += 1
		
	# ~ app.msgbox(celda.AbsoluteName)
	
	'''
	Capturar datos y copiarlos en otra hoja
	'''
	
	# ~ # A
	# ~ doc = XSCRIPTCONTEXT.getDocument()
	# ~ origen = doc.Sheets[0]
	# ~ destino = doc.Sheets[1]
	
	# ~ fila = 1
	# ~ celda_origen = origen[fila, 0]
	# ~ while celda_origen.String:
		
		# ~ fila_destino = 1
		# ~ celda_destino = destino[fila_destino, 0]
		
		# ~ while celda_destino.String:
			# ~ fila_destino += 1
			# ~ celda_destino = destino[fila_destino, 0]
			
		# ~ celda_destino.String = celda_origen.String
		# ~ celda_origen.String = ''

		# ~ fila += 1
		# ~ celda_origen = origen[fila, 0]

	# ~ app.msgbox("Los datos han sido copiados a la hoja 2")
	
	'''
	ejemplo 2:
	'''
	doc = XSCRIPTCONTEXT.getDocument()
	origen = doc.Sheets[0]
	
	celda = origen[0, 0]
	
	# ~ Value, Type, String
	app.msgbox(celda.Value)
	
	
	return
