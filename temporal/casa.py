class Casa:

	# atributo de clase
	pisos = 1
	puertas = 1
	ventanas = 0
	habitaciones = 1
	habitantes = 0
	baño = 1
	def __init__(self, tamaño:str, habitantes):
		#atributo de instancia
		#comienza a existir desde que sea crea el objeto
		self.tamaño = tamaño
		if isinstance(habitantes, int):
			if habitantes > 0:
				self.habitantes = habitantes
			

	def add_puerta(self, n: int = 1):
		if isinstance(n, int):
			if n > 0:
				self.puertas += n
			else:
				print(f"No se puede sumar {n}")
		else:
			print(f"El valor {n} no es 'inst'")
		
		
	def subs_puerta(self, n: int = 1):
		if isinstance(n, int):
			if n > 0 and self.puertas > n:
				self.puertas -= n
			else:
				print(f"No puede restar {n} a {self.puertas}")
		else:
			print(f"El valor {n} no es 'int'")


	def __repr__(self):
		class_name = type(self).__name__
		return f"{class_name}({self.tamaño},{self.habitantes})"

# Herencia

class Cabaña(Casa):
	def __init__(self, habitantes:int = 0):
		super().__init__("Pequeña",habitantes)
